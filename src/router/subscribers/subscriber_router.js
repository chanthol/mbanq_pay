import Subscriber from "../../views/subscribers/MainSubLogin"
export default {
    path: "/subscriber",
    name: 'subscriber',
    component: Subscriber,
    children: [
        {
            path: "",
            name: "subscriberLogin",
            component: () => import("../../views/subscribers/SubLogin")
        },
        {
            path: "bussiness",
            name: "bussiness",
            component: () => import("../../views/subscribers/SubBussiness")
        },
        {
            path: "personal",
            name: "personal",
            component: () => import("../../views/subscribers/SubPersonal")
        }
    ]
}
